Single Page Application Showcase
================================

Project Structure
---------
* Application is based on Spring Boot
* Deployment via docker (not yet)

TODO
----
* Make clientside bundle running
** Make grunt generated JS files avaliable to webapp
* Add tests for server and client code
* Docker container
* Maven grunt plugin
* Final goal: Just `mvn clean install` to build a single runnable docker container

Installation
------------

### Prerequisites
- Install maven
- Install npm
- `npm install -g bower`

### Compiling
Go to: `src/main/javascript` and execute:
- `npm install`
- `grunt default`

Go to: `/` and execute:
- `mvn clean install`
