module.exports = function (grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		bower: {
			install: {
				options: {
					targetDir: './lib',
					copy: false,
					layout: 'byComponent',
					install: true,
					verbose: false,
					cleanTargetDir: false,
					cleanBowerDir: false,
					bowerOptions: {}
				}
			}
		},
		browserify: {
			options: {
				transform: [ require('grunt-react').browserify ],
				browserifyOptions: {
					debug: true,
					standalone: 'server'
				}
			},
			server: {
				src: 'server.js',
				dest: 'generated/js/server-bundle.js'
			},
			client: {
				src: 'client.js',
				dest: 'public/js/client-bundle.js'
			}
		},
		uglify: {
			options: {
				mangle: true, // change variable ad function names
				sourceMap: true,
				sourceMapIncludeSources: false,
				beautify: false
			},
			client: {
				files: {
					'public/js/client-bundle.min.js': ['public/js/client-bundle.js']
				}
			}
		},
		copy: {
			dev: {
				files: [
					// includes files within path and its sub-directories
					{expand: true, src: ['public/**'], dest: '../resources'},
					{expand: true, src: ['generated/**'], dest: '../resources'}
				]
			},
			build: {
				files: [
					// includes files within path and its sub-directories
					{expand: true, src: ['public/**'], dest: '../../src/main/resources'},
					{expand: true, src: ['generated/**'], dest: '../../src/main/resources'}
				]
			}
		}
	});

	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('grunt-browserify');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-react');

	// Default task(s).
	grunt.registerTask('default', ['bower', 'browserify', 'uglify']);

	// Used during developement, to update files in src folder.
	grunt.registerTask('dev', ['bower', 'browserify', 'uglify', 'copy:dev']);


	//grunt.registerTask('watch', ['bower', 'browserify', 'uglify', 'copy:dev']);

	// Used by maven grunt plugin
	grunt.registerTask('build', ['browserify', 'uglify', 'copy:build']);

};