package org.niondir.spashowcase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;

/**
 * Created by Tarion on 23.12.2014.
 */
@Controller
public class MainController {

	private ScriptEngine engine;

	@Autowired
	private ScriptEngineManager scriptEngineManager;

	@Autowired
	private ResourceLoader resourceLoader;


	@PostConstruct
	void init() {
		engine = scriptEngineManager.getEngineByName("nashorn");
	}

	@RequestMapping("/")
	@ResponseBody
	private String home() throws ScriptException, IOException {
		loadBundle();
		return engine.eval("server.renderServerside()").toString();
	}

	private void loadBundle() throws ScriptException, IOException {
		InputStream jsStream = resourceLoader.getResource("classpath:generated/js/server-bundle.js").getInputStream();
		engine.eval("var global = this;");
		engine.eval(new InputStreamReader(jsStream));
	}

	@RequestMapping("/doit")
	public String doIt() {
		return "doit";
	}

}
